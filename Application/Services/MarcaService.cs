﻿using Application.Common;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Infra;
using Domain.Interfaces.Repository;
using Domain.Validations;
using FluentValidation.Results;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class MarcaService : ValidationAppService, IMarcaService
    {
        private readonly IMarcaRepository _MarcaRepository;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        public MarcaService(IMarcaRepository MarcaRepository, IUnitOfWork uow, IMapper mapper)
        {
            _MarcaRepository = MarcaRepository;
            _uow = uow;
            _mapper = mapper;
        }
        public async Task<IEnumerable<Marca>> GetAllAsync()
        {
            var result = await _MarcaRepository.GetAllAsync();
            return result;
        }
        public async Task<Marca> GetByIdAsync(int id)
        {
            return _mapper.Map<Marca>(await _MarcaRepository.GetByIdAsync(id));
        }
        public ValidationResult Add(Marca model)
        {
            var result = new MarcaAddValidation(_MarcaRepository).Validate(model);
            if (result.IsValid)
                _MarcaRepository.Add(model);

            _uow.Commit();
            return result;
        }

        public bool Deletar(Marca marca)
        {
            try
            {
                _MarcaRepository.Remove(marca);
                _uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<ValidationResult> Update(Marca model)
        {
            var result = new MarcaAddValidation(_MarcaRepository).Validate(model);
            if (result.IsValid)
                _MarcaRepository.Update(model);
            _uow.Commit();
            return result;
        }
    }
}
