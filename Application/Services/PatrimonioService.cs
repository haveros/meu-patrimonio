﻿using Application.Common;
using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Infra;
using Domain.Interfaces.Repository;
using Domain.Validations;
using FluentValidation.Results;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class PatrimonioService : ValidationAppService, IPatrimonioService
    {
        private readonly IPatrimonioRepository _PatrimonioRepository;
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        public PatrimonioService(IPatrimonioRepository PatrimonioRepository, IUnitOfWork uow, IMapper mapper)
        {
            _PatrimonioRepository = PatrimonioRepository;
            _uow = uow;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Patrimonio>> GetAllAsync()
        {
            var result = await _PatrimonioRepository.GetAllAsync();
            return result;
        }

        public async Task<Patrimonio> GetByIdAsync(Guid id)
        {
            return _mapper.Map<Patrimonio>(await _PatrimonioRepository.GetByIdAsync(id));
        }

        public string GerarNumeroTombo(int ultimo)
        {
            char pad = '0';
            var mes = DateTime.Now.Month < 10 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();

            var etiqueta = DateTime.Now.Year.ToString() + mes + ultimo.ToString().PadLeft(9, pad);

            return etiqueta;
        }

        public async Task<IEnumerable<Patrimonio>> GetByIdMarcaAsync(int id)
        {
            return await _PatrimonioRepository.GetByIdMarcaAsync(id);
        }

        public async Task<ValidationResult> Add(Patrimonio model)
        {
            var ultimo = _mapper.Map<int>(await _PatrimonioRepository.GetUltimoPatrimonio());
            model.NumeroTombo = GerarNumeroTombo(ultimo + 1);

            var result = new PatrimonioAddValidation(_PatrimonioRepository).Validate(model);
            if (!result.IsValid)
                return result;
            do
            {
                ultimo++;
                model.NumeroTombo = GerarNumeroTombo(ultimo);
                result = new PatrimonioAddValidation(_PatrimonioRepository).Validate(model);
            } while (!result.IsValid);


            if (result.IsValid) {                
                _PatrimonioRepository.Add(model);
            }

            _uow.Commit();
            return result;
        }

        public async Task<ValidationResult> Update(Patrimonio model)
        {
            var patrimonio = await _PatrimonioRepository.GetByIdAsync(model.PatrimonioId);
            var result = new PatrimonioUpdateValidation(_PatrimonioRepository).Validate(model);
            if (result.IsValid)
                _PatrimonioRepository.Update(model);
            _uow.Commit();
            return result;
        }

        public bool Deletar(Patrimonio model)
        {
            try
            {
                _PatrimonioRepository.Remove(model);
                _uow.Commit();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
    }
}
