﻿using Domain.Entities;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IMarcaService
    {
        Task<Marca> GetByIdAsync(int id);
        bool Deletar(Marca marca);
        Task<IEnumerable<Marca>> GetAllAsync();
        ValidationResult Add(Marca marca);
        Task<ValidationResult> Update(Marca marca);
    }
}
