﻿using Domain.Entities;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IPatrimonioService
    {
        Task<Patrimonio> GetByIdAsync(Guid id);
        bool Deletar(Patrimonio model);
        Task<IEnumerable<Patrimonio>> GetAllAsync();
        Task<IEnumerable<Patrimonio>> GetByIdMarcaAsync(int id);
        Task<ValidationResult> Add(Patrimonio marca);
        Task<ValidationResult> Update(Patrimonio marca);
    }
}
