﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Common
{
    public class ValidationAppService
    {
        private readonly ValidationResult _validationResult;
        public ValidationAppService()
        {
            _validationResult = new ValidationResult();
        }
        protected ValidationResult DomainToApplicationResult(ValidationResult result)
        {
            foreach (var erros in result.Errors)
            {
                _validationResult.Errors.Add(erros);
            }
            return _validationResult;
        }
        protected ValidationResult DomainToExceptionResult(string error)
        {
            _validationResult.Errors.Add(new ValidationFailure("ErrorException", error));
            return _validationResult;
        }
    }
}
