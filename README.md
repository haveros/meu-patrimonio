# Titulo do Projeto

Meu Patrimonio

## Começando

Clone o projeto em sua maquina local usando https://gitlab.com/haveros/meu-patrimonio.git

### Pré Requisitos


* [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
* [SQL SERVER 14.0.1000.169](https://www.microsoft.com/pt-br/sql-server/sql-server-downloads)
* [Visual Studio 2019](https://visualstudio.microsoft.com/pt-br/vs/)

### Instalação

1.  Abra o projeto com o visual studio 2019
2.  Faça o restore do banco usando o script que encontra-se no projeto infrastructure(script.sql), se preferir poderá restaurar o banco usando migrations (#Tutorial encontra-se na sessão Migrations)
3. Para configurar a string de conexão de banco va no projeto **WebApi -> AppSettings.json** localize a key DefaultConnection e coloque a instancia de sua maquina.
4.  Selecione o projeto WebApi como inicial  (Clicar com o botão direito sobre o projeto e selecionar "definir como projeto de inicialização").
5.  Aperte ctrl + f5 para inicializar o projeto sem debug, será necessario para depurar os testes

> Executar migrations
1.  Defina o projeto Infrastructure como de inicialização (Clicar com o botão direito sobre o projeto e selecionar "definir como projeto de inicialização") 
2.  va no arquivo **Infrastructure -> AppSettings.json** localize a key DefaultConnection e coloque a instancia de sua maquina.
3.  Abra o console do gerenciador de pacotes nuget e execute o comando a seguir : update-database -c ApplicationDbContext

Pronto, banco restaurado.
-----
Agora é só executar os testes automatizados do xUnit - Exemplo na sessão a seguir  

## Começando os testes

Para executar os testes automatizados com o xUnit basta seguir os passos abaixo

1. Va na camada Tests
2. Para executar testes na classe inteira, clique com o botão direito sobre a classe e selecione a opção Executar Tests para iniciar sem debug, ou depurar tests para iniciar como debug, caso queira executar apenas um método, clique com o botão direito sobre o metodo e selecione as opções de execução

### Métodos da Api

A seguir a lista de metodos que a api disponibiliza

> Marca
1.   POST | Criar Marca | http://localhost:50102/api/marca/adicionar-marca
2.   PUT | Atualizar Marca | http://localhost:50102/api/marca/atualizar-marca
3.   GET | Listar Marcas | http://localhost:50102/api/marca/listar-marcas 
4.   GET | Listar patrimonios pelo id da marca | http://localhost:50102/api/marca/{MarcaId}/listar-patrimonios
5.   GET | Listar Marcas pelo id | http://localhost:50102/api/marca/listar-marcas/{id}
6.   POST | Deletar Marca | http://localhost:50102/api/marca/deletar-marca

> Patrimonio
1.   POST | Criar Patrimonio | http://localhost:50102/api/patrimonio/adicionar-patrimonio
2.   PUT | Atualizar Patrimonio | http://localhost:50102/api/patrimonio/atualizar-patrimonio
3.   GET | Listar Patrimonios | http://localhost:50102/api/patrimonio/listar-patrimonios
4.   GET | Listar Patrimonios pelo id | http://localhost:50102/api/patrimonio/listar-patrimonios/{id}
5.   GET | Deletar Patrimonio | http://localhost:50102/api/patrimonio/deletar-patrimonio

### Arquitetura

A arquitetura do sistema foi feita utilizando o padrão DDD, e foi dividida nas seguintes camadas:

>  Application
*  Responsável por aplicar as regras de negócio do sistema
>  Domain
*  Normalmente definido através do padrão Domain Model, essa camada é a base de todo o DDD. É aqui que precisamos definir corretamente nosso modelo de negócios em termos de classes, do contrário teremos problemas posteriores em nosso projeto
> Infrastructure
*  Os repositórios de dados são definidos através de um padrão: o padrão Repository. A ideia é que o Modelo de domínio esteja livre de qualquer definição de infraestrutura de dados, fazendo com que ele siga os princípios POCO (Plain Old Common-Runtime Object) e PI (Persistence Ignorance) – termos que são utilizados em conjunto para indicar objetos que não possuem comportamentos (métodos) definidos (entidades).
> Ioc
*  Camada responsavel por controlar as transações da aplicação
> Tests
*  Camada de tests, desenvolvida seguindo de acordo com o TDD 
> WebApi
*  Camada responsavel por controlar as requisições e mandar aos seus devidos serviços, retornando uma resposta HTTP

## Versão

Versão 1.0

## Autor

* **Mateus Primo Cardoso**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
