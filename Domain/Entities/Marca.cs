﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Marca
    {
        public Marca()
        {
            Patrimonios = new HashSet<Patrimonio>();
        }

        public int MarcaId { get; set; }
        public string Nome { get; set; }
        public ICollection<Patrimonio> Patrimonios { get; set; }
    }
}
