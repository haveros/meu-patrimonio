﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Patrimonio
    {
        public Guid PatrimonioId { get; set; }
        public int MarcaId { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string NumeroTombo { get; set; }

        public Marca Marca { get; set; }
    }
}
