﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repository
{
    public interface IMarcaRepository : IBaseRepository<Marca>, IDapperRepository<Marca, int>
    {
        Task<IEnumerable<Marca>> GetAllByNome(string nome);
    }

}
