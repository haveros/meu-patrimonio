﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repository
{
    public interface IPatrimonioRepository : IBaseRepository<Patrimonio>, IDapperRepository<Patrimonio, Guid>
    {
        Task<IEnumerable<Patrimonio>> GetByIdMarcaAsync(int id);
        Task<Patrimonio> GetByNumeroTomboAsync(string id);
        Task<int> GetUltimoPatrimonio();
    }
}
