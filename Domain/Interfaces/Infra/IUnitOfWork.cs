﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces.Infra
{
    public interface IUnitOfWork
    {
        void Commit();
        void BeginTransaction();
        void BeginCommit();
        void BeginRollback();
    }
}
