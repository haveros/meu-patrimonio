﻿using Domain.Interfaces.Repository;
using FluentValidation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validations
{
    public class PatrimonioAddValidation : AbstractValidator<Entities.Patrimonio>
    {
        protected IPatrimonioRepository _PatrimonioRepository;
        protected const string _mensagemCampoObrigatorio = "Campo Obrigatório";
        public PatrimonioAddValidation(IPatrimonioRepository PatrimonioRepository)
        {
            _PatrimonioRepository = PatrimonioRepository;

            RuleFor(x => x.Nome).NotNull().WithMessage(_mensagemCampoObrigatorio);
            RuleFor(x => x.MarcaId).NotNull().WithMessage(_mensagemCampoObrigatorio);

            RuleFor(Patrimonio => Patrimonio)
               .Must(VerificaEtiquetaPatrimonio)
               .WithMessage("Número do tombo já existe.");

        }

        private bool VerificaEtiquetaPatrimonio(Entities.Patrimonio Patrimonio)
        {

            var model = _PatrimonioRepository.GetByNumeroTomboAsync(Patrimonio.NumeroTombo).Result;

            if (model != null && model.NumeroTombo == Patrimonio.NumeroTombo || model == null)
                return true;

            return false;
        }
    }
}
