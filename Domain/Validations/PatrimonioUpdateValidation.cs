﻿using Domain.Interfaces.Repository;
using FluentValidation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validations
{
    public class PatrimonioUpdateValidation : AbstractValidator<Entities.Patrimonio>
    {
        protected IPatrimonioRepository _PatrimonioRepository;
        protected const string _mensagemCampoObrigatorio = "Campo Obrigatório";
        public PatrimonioUpdateValidation(IPatrimonioRepository PatrimonioRepository)
        {
            _PatrimonioRepository = PatrimonioRepository;

            RuleFor(p => p)
               .Must(VerificaIdPatrimonio)
               .WithMessage("Patrimonio não encontrado.");

            RuleFor(p => p)
               .Must(VerificaNomePatrimonio)
               .WithMessage("Número do tombo não pode ser alterado.");
        }

        private bool VerificaNomePatrimonio(Entities.Patrimonio Patrimonio)
        {

            var model = _PatrimonioRepository.GetByIdAsync(Patrimonio.PatrimonioId).Result;

            if (model != null && model.NumeroTombo == Patrimonio.NumeroTombo)
                return true;

            return false;
        }
        private bool VerificaIdPatrimonio(Entities.Patrimonio Patrimonio)
        {

            var model = _PatrimonioRepository.GetByIdAsync(Patrimonio.PatrimonioId).Result;

            if (model == null)
                return false;

            return true;
        }
    }
}
