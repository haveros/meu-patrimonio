﻿using Domain.Interfaces.Repository;
using FluentValidation;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validations
{
    public class MarcaAddValidation : AbstractValidator<Entities.Marca>
    {
        protected IMarcaRepository _MarcaRepository;
        protected const string _mensagemCampoObrigatorio = "Campo Obrigatório";
        public MarcaAddValidation(IMarcaRepository MarcaRepository)
        {
            _MarcaRepository = MarcaRepository;

            RuleFor(x=> x.Nome).NotNull().WithMessage(_mensagemCampoObrigatorio);
            
            RuleFor(marca => marca)
               .Must(VerificaNomeMarca)
               .WithMessage("Nome já cadastrado.");
        }

        private bool VerificaNomeMarca(Entities.Marca marca)
        {

            var model = _MarcaRepository.GetAllByNome(marca.Nome).Result;
            
            if (model.Count() == 0 || marca.MarcaId > 0)
                return true;

            return false;
        }
    }
}
