﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarcaController : ControllerBase
    {
        private readonly IMarcaService _marcaService;
        private readonly IPatrimonioService _patrimonioService;
        public MarcaController(IMarcaService marcaService, IPatrimonioService patrimonioService)
        {
            _marcaService = marcaService;
            _patrimonioService = patrimonioService;
        }

        [HttpGet("listar-marcas")]
        public async Task<JsonResult> GetAllMarcasAsync()
        {
            var model = await _marcaService.GetAllAsync();
            return new JsonResult(model);
        }

        [HttpGet("listar-marcas/{id}")]
        public async Task<JsonResult> GetMarcaByIdAsync(int id)
        {
            var model = await _marcaService.GetByIdAsync(id);
            return new JsonResult(model);
        }

        [HttpGet("{id}/listar-patrimonios")]
        public async Task<JsonResult> GetPatrimoniosByIdMarcaAsync(int id)
        {
            var model = await _patrimonioService.GetByIdMarcaAsync(id);
            return new JsonResult(model);
        }

        [HttpPost("adicionar-marca")]
        public JsonResult AddMarca(Marca marca)
        {
            var resultado = _marcaService.Add(marca);

            if (resultado.IsValid)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.Created };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = resultado.Errors.FirstOrDefault().ToString() };
        }

        [HttpPut("atualizar-marca")]
        public async Task<JsonResult> UpdateMarca(Marca marca)
        {
            var resultado = await _marcaService.Update(marca);

            if (resultado.IsValid)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.Created };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = resultado.Errors.FirstOrDefault().ToString() };
        }

        [HttpPost("deletar-marca")]
        public JsonResult DeletarMarca(Marca marca)
        {
            var resultado = _marcaService.Deletar(marca);

            if (resultado)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.OK };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = "Erro ao excluir marca, favor deletar os patrimonios relacionados." };
        }
    }
}