﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatrimonioController : ControllerBase
    {
        private readonly IPatrimonioService _patrimonioService;
        public PatrimonioController(IPatrimonioService patrimonioService)
        {
            _patrimonioService = patrimonioService;
        }
        [HttpGet("listar-patrimonios")]
        public async Task<JsonResult> GetAllPatrimoniosAsync()
        {
            var model = await _patrimonioService.GetAllAsync();
            return new JsonResult(model);
        }

        [HttpGet("listar-patrimonios/{id}")]
        public async Task<JsonResult> GetPatrimonioByIdAsync(Guid id)
        {
            var model = await _patrimonioService.GetByIdAsync(id);
            return new JsonResult(model);
        }

        [HttpPost("adicionar-patrimonio")]
        public async Task<JsonResult> AddPatrimonio(Patrimonio patrimonio)
        {
            var resultado = await _patrimonioService.Add(patrimonio);

            if (resultado.IsValid)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.Created };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = resultado.Errors.FirstOrDefault().ToString() };
        }

        [HttpPut("atualizar-patrimonio")]
        public async Task<JsonResult> UpdatePatrimonio(Patrimonio patrimonio)
        {
            var resultado = await _patrimonioService.Update(patrimonio);

            if (resultado.IsValid)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.Created };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = resultado.Errors.FirstOrDefault().ToString() };
        }

        [HttpPost("deletar-patrimonio")]
        public JsonResult DeletarPatrimonio(Patrimonio patrimonio)
        {
            var resultado = _patrimonioService.Deletar(patrimonio);

            if (resultado)
                return new JsonResult(resultado) { StatusCode = (int)HttpStatusCode.OK };
            else
                return new JsonResult(false) { StatusCode = (int)HttpStatusCode.BadRequest, Value = "Erro ao excluir patrimonio, favor deletar os patrimonios relacionados." };
        }
    }
}