﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.AutoMapper;
using Application.Interfaces;
using Application.Services;
using AutoMapper;
using Domain.Interfaces.Infra;
using Domain.Interfaces.Repository;
using Infrastructure.Data;
using Infrastructure.Repository;
using Infrastructure.Uow;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Ioc.Configuration
{
    public class InjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            #region Contexts
            services.AddDbContext<ApplicationDbContext>();
            services.AddScoped<DapperContext>();
            #endregion
            #region AutoMapper

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainToViewModelMappingProfile());
                mc.AddProfile(new ViewModelToDomainMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion
            #region Infra
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMarcaRepository, MarcaRepository>();
            services.AddScoped<IPatrimonioRepository, PatrimonioRepository>();
            #endregion
            #region Service
            services.AddScoped<IMarcaService, MarcaService>();
            services.AddScoped<IPatrimonioService, PatrimonioService>();
            #endregion
        }
    }
}
