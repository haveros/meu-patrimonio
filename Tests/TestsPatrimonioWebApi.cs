﻿using Application.Interfaces;
using Application.Services;
using Domain.Entities;
using Domain.Interfaces.Infra;
using Infrastructure.Data;
using Infrastructure.Uow;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class TestsPatrimonioWebApi
    {
        #region Testes Patrimonio
        [Fact]
        private void CriarPatrimonio()
        {
            string URL = "http://localhost:50102/api/patrimonio/adicionar-patrimonio";
            TestsMarcaWebApi marcaTest = new TestsMarcaWebApi();

            var marcas = marcaTest.ListarMarcas();

            Patrimonio patrimonio = new Patrimonio();
            patrimonio.Nome = "Teste";
            patrimonio.MarcaId = marcas.Count() > 0 ? marcas.FirstOrDefault().MarcaId : 0;
            patrimonio.Descricao = "Mouse";


            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(patrimonio);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.Created)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }

        [Fact]
        private void AtualizarPatrimonio()
        {
            string URL = "http://localhost:50102/api/patrimonio/atualizar-patrimonio";
            Patrimonio patrimonio = ListarPatrimonios().FirstOrDefault();

            if (patrimonio == null)
            {
                Assert.True(false);
                return;
            }

            patrimonio.Nome = "Teste Atualização";

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(patrimonio);

                    streamWriter.Write(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.Created)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }

        [Fact]
        private IEnumerable<Patrimonio> ListarPatrimonios()
        {
            var requisicaoWeb = (HttpWebRequest)WebRequest.Create("http://localhost:50102/api/patrimonio/listar-patrimonios");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";
            IEnumerable<Patrimonio> patrimonios = new List<Patrimonio>();

            try
            {
                using (var resposta = requisicaoWeb.GetResponse())
                {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    Console.Write(objResponse);
                    streamDados.Close();
                    resposta.Close();

                    Assert.True(true);
                    patrimonios = JsonConvert.DeserializeObject<IEnumerable<Patrimonio>>(objResponse.ToString());
                }

            }
            catch (Exception e)
            {
                Assert.True(false);
            }
            return patrimonios;
        }

        [Fact]
        private Patrimonio ListarPatrimonioById()
        {
            var patrimonios = ListarPatrimonios();
            if (patrimonios == null) 
            {
                Assert.True(false);
                return null;
            }

            var id = patrimonios.FirstOrDefault().PatrimonioId;
            var requisicaoWeb = (HttpWebRequest)WebRequest.Create($"http://localhost:50102/api/patrimonio/listar-patrimonios/{id}");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";
            Patrimonio patrimonio = new Patrimonio();

            try
            {
                using (var resposta = requisicaoWeb.GetResponse())
                {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    Console.Write(objResponse);
                    streamDados.Close();
                    resposta.Close();

                    Assert.True(true);
                    patrimonio = JsonConvert.DeserializeObject<Patrimonio>(objResponse.ToString());
                }

            }
            catch (Exception e)
            {
                Assert.True(false);
            }
            return patrimonio;
        }

        [Fact]
        private void DeletarPatrimonio()
        {
            string URL = "http://localhost:50102/api/patrimonio/deletar-patrimonio";
            Patrimonio patrimonio = ListarPatrimonios().FirstOrDefault();

            if (patrimonio == null)
            {
                Assert.True(false);
                return;
            }

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(patrimonio);

                    streamWriter.Write(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }
        #endregion
    }
}