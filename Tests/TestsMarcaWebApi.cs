﻿using Application.Interfaces;
using Application.Services;
using Domain.Entities;
using Domain.Interfaces.Infra;
using Infrastructure.Data;
using Infrastructure.Uow;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class TestsMarcaWebApi
    {
        #region Testes Marca
        [Fact]
        private void CriarMarca()
        {
            string URL = "http://localhost:50102/api/marca/adicionar-marca";
            Marca marca = new Marca();
            marca.Nome = "Teste x";

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(marca);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.Created)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }

        [Fact]
        private void AtualizarMarca()
        {
            string URL = "http://localhost:50102/api/marca/atualizar-marca";
            var marcas = ListarMarcas();

            if (marcas.Count() == 0)
            {
                Assert.True(false);
                return;
            }

            var marca = marcas.FirstOrDefault();
            marca.Nome = "Teste Atualização";

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "PUT";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(marca);

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.Created)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }

        [Fact]
        public IEnumerable<Marca> ListarMarcas()
        {
            var requisicaoWeb = (HttpWebRequest)WebRequest.Create("http://localhost:50102/api/marca/listar-marcas");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";
            IEnumerable<Marca> marcas = new List<Marca>();

            try
            {
                using (var resposta = requisicaoWeb.GetResponse())
                {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    Console.Write(objResponse);
                    streamDados.Close();
                    resposta.Close();

                    Assert.True(true);
                    marcas = JsonConvert.DeserializeObject<IEnumerable<Marca>>(objResponse.ToString());
                }

            }
            catch (Exception e)
            {
                Assert.True(false);
            }
            return marcas;
        }

        [Fact]
        public IEnumerable<Patrimonio> ListarPatrimoniosByIdMarca()
        {
            TestsMarcaWebApi testsMarca = new TestsMarcaWebApi();
            var marcas = testsMarca.ListarMarcas();

            if (marcas.Count() == 0)
            {
                Assert.True(false);
                return null;
            }

            var requisicaoWeb = (HttpWebRequest)WebRequest.Create($"http://localhost:50102/api/marca/{marcas.FirstOrDefault().MarcaId}/listar-patrimonios");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";

            IEnumerable<Patrimonio> patrimonios = new List<Patrimonio>();

            try
            {
                using (var resposta = requisicaoWeb.GetResponse())
                {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    Console.Write(objResponse);
                    streamDados.Close();
                    resposta.Close();

                    Assert.True(true);
                    patrimonios = JsonConvert.DeserializeObject<IEnumerable<Patrimonio>>(objResponse.ToString());
                }

            }
            catch (Exception e)
            {
                Assert.True(false);
            }
            return patrimonios;
        }

        [Fact]
        private Marca ListarMarcaById()
        {
            var marcas = ListarMarcas();

            if (marcas.Count() == 0)
            {
                Assert.True(true);
                return null;
            }

            var id = marcas.FirstOrDefault().MarcaId;
            var requisicaoWeb = (HttpWebRequest)WebRequest.Create($"http://localhost:50102/api/marca/listar-marcas/{id}");
            requisicaoWeb.Method = "GET";
            requisicaoWeb.UserAgent = "RequisicaoWebDemo";
            Marca marca = new Marca();

            try
            {
                using (var resposta = requisicaoWeb.GetResponse())
                {
                    var streamDados = resposta.GetResponseStream();
                    StreamReader reader = new StreamReader(streamDados);
                    object objResponse = reader.ReadToEnd();
                    Console.Write(objResponse);
                    streamDados.Close();
                    resposta.Close();

                    Assert.True(true);
                    marca = JsonConvert.DeserializeObject<Marca>(objResponse.ToString());
                }

            }
            catch (Exception e)
            {
                Assert.True(false);
            }
            return marca;
        }

        [Fact]
        private void DeletarMarca()
        {
            string URL = "http://localhost:50102/api/marca/deletar-marca";
            var marcas = ListarMarcas();
            if (marcas.Count() == 0)
            {
                Assert.True(false);
                return;
            }

            var marca = marcas.FirstOrDefault();

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new

                StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(marca);

                    streamWriter.Write(json);
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();

                }

                if (httpResponse.StatusCode == HttpStatusCode.OK)
                    Assert.True(true);
                else
                {
                    Console.WriteLine(httpResponse.ContentEncoding);
                    Assert.True(false);
                }
            }
            catch (Exception e)
            {
                Assert.True(false);
            }
        }
        #endregion
    }
}