USE [master]
GO
/****** Object:  Database [MeuPatrimonio]    Script Date: 13/01/2020 15:20:54 ******/
CREATE DATABASE [MeuPatrimonio]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MeuPatrimonio', FILENAME = N'D:\Arquivos e Programas\MICROSOFT SQL SERVER\MSSQL14.MSSQLSERVER\MSSQL\DATA\MeuPatrimonio.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'MeuPatrimonio_log', FILENAME = N'D:\Arquivos e Programas\MICROSOFT SQL SERVER\MSSQL14.MSSQLSERVER\MSSQL\DATA\MeuPatrimonio_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [MeuPatrimonio] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MeuPatrimonio].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MeuPatrimonio] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET ARITHABORT OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MeuPatrimonio] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MeuPatrimonio] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MeuPatrimonio] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MeuPatrimonio] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [MeuPatrimonio] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET RECOVERY FULL 
GO
ALTER DATABASE [MeuPatrimonio] SET  MULTI_USER 
GO
ALTER DATABASE [MeuPatrimonio] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MeuPatrimonio] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MeuPatrimonio] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MeuPatrimonio] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MeuPatrimonio] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MeuPatrimonio', N'ON'
GO
ALTER DATABASE [MeuPatrimonio] SET QUERY_STORE = OFF
GO
USE [MeuPatrimonio]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 13/01/2020 15:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marca]    Script Date: 13/01/2020 15:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marca](
	[MarcaId] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](max) NULL,
 CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED 
(
	[MarcaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patrimonio]    Script Date: 13/01/2020 15:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patrimonio](
	[PatrimonioId] [uniqueidentifier] NOT NULL,
	[MarcaId] [int] NOT NULL,
	[Nome] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[NumeroTombo] [nvarchar](max) NULL,
 CONSTRAINT [PK_Patrimonio] PRIMARY KEY CLUSTERED 
(
	[PatrimonioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_Patrimonio_MarcaId]    Script Date: 13/01/2020 15:20:54 ******/
CREATE NONCLUSTERED INDEX [IX_Patrimonio_MarcaId] ON [dbo].[Patrimonio]
(
	[MarcaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Patrimonio]  WITH CHECK ADD  CONSTRAINT [FK_Patrimonio_Marca_MarcaId] FOREIGN KEY([MarcaId])
REFERENCES [dbo].[Marca] ([MarcaId])
GO
ALTER TABLE [dbo].[Patrimonio] CHECK CONSTRAINT [FK_Patrimonio_Marca_MarcaId]
GO
USE [master]
GO
ALTER DATABASE [MeuPatrimonio] SET  READ_WRITE 
GO
