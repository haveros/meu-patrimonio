﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

namespace Infrastructure.Extensions
{
    public static class DbContextExtentions
    {
        /// <summary>
        /// Verifica se um objeto esta trackeado no contexto do entity.
        /// https://stackoverflow.com/questions/47813464/delete-loaded-and-unloaded-objects-by-id-in-entityframeworkcore
        /// </summary>
        /// <typeparam name="TEntity">O tipo da entidade.</typeparam>
        /// <param name="context">A instância do DbContext.</param>
        /// <param name="entity">O objeto que se deseja verificar.</param>
        /// <returns>A instância trackeada caso exista ou null.</returns>
        public static TEntity FindTracked<TEntity>(this DbContext context, TEntity entity)
            where TEntity : class
        {
            var entityType = context.Model.FindEntityType(typeof(TEntity));
            var key = entityType.FindPrimaryKey();

            //Obtem o valor de cada propriedade da primary key via reflection
            var keyValues = key.Properties.Select(x => entity.GetType().GetProperty(x.Name).GetValue(entity, null)).ToArray();

            var stateManager = context.GetDependencies().StateManager;
            var entry = stateManager.TryGetEntry(key, keyValues);
            return entry?.Entity as TEntity;
        }
    }
}
