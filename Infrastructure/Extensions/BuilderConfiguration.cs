﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Infrastructure.Extensions
{
    public class BuilderConfiguration
    {
        public static IConfigurationRoot AddConfigurationDirectory()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
        }
    }
}
