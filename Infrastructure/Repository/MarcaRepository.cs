﻿using Dapper;
using Domain.Entities;
using Domain.Interfaces.Repository;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class MarcaRepository : BaseRepository<Marca>, IMarcaRepository
    {
        private readonly DapperContext _dapperContext;
        public MarcaRepository(ApplicationDbContext context, DapperContext dapperContext) : base(context)
        {
            _dapperContext = dapperContext;
        }

        public async Task<Marca> GetByIdAsync(int id)
        {
            var query = "SELECT * FROM Marca WHERE MarcaId = @id";
            return await _dapperContext.Connection.QueryFirstOrDefaultAsync<Marca>(query, new { id });
        }
        public async Task<IEnumerable<Marca>> GetAllByNome(string nome)
        {
            var query = @"SELECT * FROM Marca WHERE Nome = @nome";
            return await _dapperContext.Connection.QueryAsync<Marca>(query, new { nome});
        }
        public async Task<IEnumerable<Marca>> GetAllAsync()
        {
            var query = @"SELECT * FROM Marca";
            return await _dapperContext.Connection.QueryAsync<Marca>(query);
        }
    }
}
