﻿using System;
using System.Collections.Generic;
using Domain.Interfaces.Repository;
using Infrastructure.Data;
using Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationDbContext _context;
        protected DbSet<TEntity> DbSet;

        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
            DbSet = _context.Set<TEntity>();
        }
        public TEntity Add(TEntity obj)
        {
            return DbSet.Add(obj).Entity;
        }

        public virtual void AddRange(IEnumerable<TEntity> obj)
        {
            DbSet.AddRange(obj);
        }

        public virtual void Remove(TEntity obj)
        {
            DbSet.Remove(_context.FindTracked(obj) ?? obj);
        }

        public virtual void Update(TEntity obj)
        {
            DbSet.Update(obj);
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
