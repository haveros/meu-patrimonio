﻿using Dapper;
using Domain.Entities;
using Domain.Interfaces.Repository;
using Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class PatrimonioRepository : BaseRepository<Patrimonio>, IPatrimonioRepository
    {
        private readonly DapperContext _dapperContext;
        public PatrimonioRepository(ApplicationDbContext context, DapperContext dapperContext) : base(context)
        {
            _dapperContext = dapperContext;
        }

        public async Task<Patrimonio> GetByNumeroTomboAsync(string numero)
        {
            var query = "SELECT * FROM Patrimonio WHERE NumeroTombo = @numero";
            return await _dapperContext.Connection.QueryFirstOrDefaultAsync<Patrimonio>(query, new { numero });
        }

        public async Task<Patrimonio> GetByIdAsync(Guid id)
        {
            var query = "SELECT * FROM Patrimonio WHERE PatrimonioId = @id";
            return await _dapperContext.Connection.QueryFirstOrDefaultAsync<Patrimonio>(query, new { id });
        }

        public async Task<int> GetUltimoPatrimonio()
        {
            var query = "SELECT COUNT(PatrimonioId) as numero FROM Patrimonio";
            return await _dapperContext.Connection.QueryFirstOrDefaultAsync<int>(query);
        }

        public async Task<IEnumerable<Patrimonio>> GetAllAsync()
        {
            var query = @"SELECT * FROM Patrimonio";
            return await _dapperContext.Connection.QueryAsync<Patrimonio>(query);
        }

        public async Task<IEnumerable<Patrimonio>> GetByIdMarcaAsync(int id)
        {
            var query = @"SELECT * FROM Patrimonio where MarcaId = @id";
            return await _dapperContext.Connection.QueryAsync<Patrimonio>(query, new { id });
        }
    }
}
