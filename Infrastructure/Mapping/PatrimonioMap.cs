﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mapping
{
    public class PatrimonioMap : IEntityTypeConfiguration<Patrimonio>
    {
        public void Configure(EntityTypeBuilder<Patrimonio> builder)
        {
            builder.Property(e => e.PatrimonioId).ValueGeneratedOnAdd();
            builder.Property(e=> e.NumeroTombo).ValueGeneratedOnAdd();

            builder.HasOne(d => d.Marca)
                .WithMany(p => p.Patrimonios)
                .HasForeignKey(d => d.MarcaId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
